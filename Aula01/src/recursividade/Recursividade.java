package recursividade;

public class Recursividade {
	
private int sucessor(int n) {
		
		int s = n + 1;
		return s;
	}

	private int antecessor(int n){
		
		int s = n - 1 ;
		return s;
	}

	private int soma(int n, int m) {
		
		if(m == 0) {
			
			return n;
		} else {
			
		   return sucessor(soma(m-1, n));
		}
	}
	
	private int subtrai(int n, int m) {
		
		if(n == 0) {
			
			return m;
		} else {
			
			return antecessor(subtrai(n-1, m));
		}
	}

	private int multiplica(int n, int m) {
		
		if(n == 0) {
			
			return 0;
		} else {
			
			return soma(multiplica(n-1,m), m);
		}
	}
	
	private int div(int n, int m) {
        
        if (m == 0 || n < m){
        	
            return 0;
        } else if (n - m == 0){
        	
            return 1;
        } else {
        	
            return sucessor(div(n - m, m));
        }
    }
			
	private int pot(int n, int m) {
        
        if (m == 1){
        
            return n;
        } else {

            return  multiplica(pot(n, m-1),n);
        }
    }
	
	private int resto(int n, int m) {
       
        if (n < m){
        	
            return n;
        } else {
     
            return resto(n - m, m);      
        }
    }

	public static void main(String[] args){
		
		Recursividade exercicio = new Recursividade();
		
		System.out.println("Soma: " + exercicio.soma(5, 5));
		System.out.println("Subtracao: " + exercicio.subtrai(10, 6));
		System.out.println("Multiplicacao: " + exercicio.multiplica(10, 10));
		System.out.println("Divis�o: " + exercicio.div(20,2));
		System.out.println("Potenciacao: " +  exercicio.pot(3, 2));
		System.out.println("Resto: " +  exercicio.resto(20, 11));																		
	}

}
