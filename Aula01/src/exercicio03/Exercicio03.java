package exercicio03;

import java.util.Scanner;

public class Exercicio03 {
	
	public static void main(String[] args) {
		int operacao = 0;
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Entre com primeiro numero: ");
		int n1 = Integer.parseInt(scanner.nextLine());
		
		System.out.print("Entre com segundo numero: ");
		int n2 = Integer.parseInt(scanner.nextLine());
		
		System.out.print("Qual operacao + ou -: ");
		String op = scanner.nextLine();
				
		if(op.equals ("+"))  { 
			operacao = n1 + n2;
		} else if(op.equals ("-")){
			operacao = n1 - n2;
		} 
		
		System.out.println("Resultado: " + operacao);
		scanner.close();
	}

}
