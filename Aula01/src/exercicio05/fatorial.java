package exercicio05;

import java.util.Scanner;

public class fatorial {
public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
    int num, fat = 1;

	System.out.print("Digite um n: ");
    num = scanner.nextInt();
        
    for(int i = 1;i <= num; i++){
		fat = fat * i;
    }
        
	System.out.println("!" + num + " = " + fat);
	scanner.close();
}
}
