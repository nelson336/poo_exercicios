package nomes;

import java.util.Scanner;

public class Nomes {
public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Digite um nome: ");
		String nome1 = scanner.nextLine();
		System.out.print("Digite outro nome: ");
		String nome2 = scanner.nextLine();
		System.out.println("Nomes digitados: " + nome1 + " e " + nome2);
		scanner.close();
	}

}
