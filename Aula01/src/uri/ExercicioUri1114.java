package uri;

import java.io.IOException;

import java.util.*;
public class ExercicioUri1114 {
	
	public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int senha;
        senha = Integer.parseInt(sc.nextLine());
        while(senha!=2002){
            System.out.println("Senha Invalida");
            senha = Integer.parseInt(sc.nextLine());
        }
        System.out.println("Acesso Permitido");
        sc.close();
    }
}