package uri;

import java.io.IOException;
import java.util.*;

public class ExercicioUri1154 {
	 
	public static void main(String[] args) throws IOException {
		
        Scanner scan = new Scanner(System.in);
        int age = scan.nextInt(), qtd = 0;
        double soma = 0;
        while(age >= 0){
            soma += age;
            qtd++;
            age = scan.nextInt();
        }
        System.out.printf("%.2f\n", soma/qtd);
        scan.close();
    }
}