package uri;

import java.io.IOException;
import java.util.*;

public class ExercicioUri1589 {
	 
	public static void main(String[] args) throws IOException {
    	
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt(), r1, r2; 
        while(n-- > 0){
            r1 = scan.nextInt();
            r2 = scan.nextInt();
            System.out.println(r1 + r2);
        }
        scan.close();
    }
}
