package uri;

import java.util.Scanner;


public class ExercicioUri1924{
	
    static Scanner scan = new Scanner(System.in);
    
    public static void main(String[] args){
        int n = scan.nextInt();
        
        @SuppressWarnings("unused")
		String s;
        s = scan.nextLine();
        while(n-- > 0)
            s = scan.nextLine();   
        System.out.println("Ciencia da Computacao");
    }
}