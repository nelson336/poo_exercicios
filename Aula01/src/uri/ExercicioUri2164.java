package uri;
import java.util.Scanner;

public class ExercicioUri2164 {

	public static void main(String[] args) {

		Scanner ler = new Scanner(System.in);
		int n = ler.nextInt();
		if(n > 0 && n <= 50)
		{
			double fib = (Math.pow(((1 + Math.sqrt(5)) / 2), n) - Math.pow(((1 - Math.sqrt(5)) / 2), n)) / Math.sqrt(5);
			String fibonacci = String.format("%.1f", fib);
			System.out.println(fibonacci); 
		}
		ler.close();
	}

}
