package uri;

import java.io.IOException;
import java.util.*;

public class ExercicioUri1759 {
	 
	public static void main(String[] args) throws IOException {
		
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt(), i = 1; 
        while(i<n){
            System.out.print("Ho ");
            i++;
        }
        System.out.println("Ho!");
        scan.close();
    }
}