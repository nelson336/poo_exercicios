package uri;

import java.io.IOException;
import java.util.Scanner;

public class ExercicioUri1005 {
	
	public static void main(String[] args) throws IOException {
		Scanner scan = new Scanner(System.in);
		double a, b, med;
		a = Double.parseDouble(scan.nextLine()); 
		b = Double.parseDouble(scan.nextLine()); 
		med = ((a * 3.5) + (b * 7.5))/11;
		System.out.printf("MEDIA = %.5f\n", med);
		
		scan.close();
	}
}
