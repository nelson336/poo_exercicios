package media;

import java.util.Scanner;

public class CalcularMediaAlunos {
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int qtd = 0;
		double media = 0;
		
		while(true) {
			qtd++;
			System.out.print("Digite uma nota: ");
			media += Double.parseDouble(sc.nextLine());
			System.out.println("Deseja sair? (S/N)");
			if(sc.nextLine().equalsIgnoreCase("s")) {
				break;
			}
		}
		
		media = media/qtd;
		System.out.println("A media dos alunos �: " + media);
	}

}
