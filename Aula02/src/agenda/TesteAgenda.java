package agenda;

import java.util.LinkedList;
import java.util.List;

public class TesteAgenda {
	
	public static void main(String[] args) {
		
		Agenda agenda = new Agenda();
		
		List<Telefone> telefones = new LinkedList<>();
		
		telefones.add(new Telefone("Cel", 1234564));
		
		agenda.addContato(new Contato(1, "Fulano de tal", new Endereco("Rua Teste 01", 565, "Teste", 38300078), telefones));
		agenda.addContato(new Contato(2, "Beltrano", new Endereco("Rua Teste 02", 565, "Teste", 38300078), telefones));

		
		agenda.selectAll();
		agenda.remove(1);
		agenda.selectAll();
		Contato contato = agenda.getContato(1);
		contato.mostra();
	}
}
