package retangulo;

public class Retangulo {

	private double width = 1;
	private double height = 1;

	public Retangulo() {

	}

	public Retangulo(double width, double height) {

		if ((width >= 0 && width <= 20 ) && (height >= 0 && height <= 20 ) ) {
			this.width = width;
			this.height = height;
		}else {
			System.out.println("Valor inv�lido (0 a 20)");
		}
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {

		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double area() {
		return width * height;
	}

	public double perimetro() {
		return (height * 2) + (width * 2);
	}

	public void mostra() {
		System.out.println("altura: " + height + " largura: " + width + " area: " + (width * height) + " m"
				+ " perimetro: " + ((height * 2) + (width * 2)));
	}

}
