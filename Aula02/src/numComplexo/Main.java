package numComplexo;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Entre num1: ");
		double n1 = Double.parseDouble(sc.nextLine());
		
		System.out.print("Entre num2: ");
		double n2 = Double.parseDouble(sc.nextLine());
		
		System.out.print("Entre num3: ");
		double n3 = Double.parseDouble(sc.nextLine());
		System.out.print("Entre num4: ");
		double n4 = Double.parseDouble(sc.nextLine());
		
		NumComplexo numComplexo = new NumComplexo(n1, n2);
		NumComplexo numComplexo2 = new NumComplexo(n3, n4);
		
		NumComplexo numComplexoSoma = numComplexo.soma(numComplexo2);
		NumComplexo numComplexoSubtrai = numComplexo.subtrai(numComplexo2);
		
		System.out.println("N1 " + numComplexo);
		System.out.println("N2 " + numComplexo2);
		System.out.println("Soma: " + numComplexoSoma);
		System.out.println("Subtrai " + numComplexoSubtrai);
		sc.close();
				
	}
	
	
}
