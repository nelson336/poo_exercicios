package numComplexo;

public class NumComplexo {
	
	private double pReal;
	private double pImage;
	public double getpReal() {
		return pReal;
	}
	public void setpReal(double pReal) {
		this.pReal = pReal;
	}
	public double getpImage() {
		return pImage;
	}
	public void setpImage(double pImage) {
		this.pImage = pImage;
	}
	public NumComplexo(double pReal, double pImage) {
		this.pReal = pReal;
		this.pImage = pImage;
	}
	
	
	public NumComplexo() {
	}
	
	public NumComplexo soma(NumComplexo n){
		double newPReal = this.pReal + n.pReal;
		double newPimage = this.pImage + n.pImage;
		return new NumComplexo(newPReal, newPimage);
	}
	
	public NumComplexo subtrai(NumComplexo n){
		double newPReal = this.pReal - n.pReal;
		double newPimage = this.pImage - n.pImage;
		return new NumComplexo(newPReal, newPimage);
	}
	
	@Override
	public String toString() {
		
		
		return  String.format("%.2fi %s %.2fi", +pReal, pImage < 0 ? "-" : "+",  +pImage);
	}
	
}
