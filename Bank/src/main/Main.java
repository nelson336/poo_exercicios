package main;

import java.math.BigDecimal;
import java.util.Scanner;

import controller.Controller;
import model.BankData;
import model.Client;

public class Main {

	private Controller controller = new Controller();
	private Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	
		Main test = new Main();
		
		
		test.mock();
		
		
		
		while (true) {
			test.menu();
			System.out.print("Escolha uma opcao: ");
			Integer op = test.readInt();
			Menu menuSelected = Menu.getOptionToMenu(op);
			
			switch (menuSelected) {
			case  CAD_CLIENT:
				test.registerClient();
				break;
			case LIST_CLIENT:
				test.selectAllClient();
				break;
			case SEARCH_CLIENT_CPF:
				test.searchClientByCPF();
				break;
			case DELETE_CLIENT:
				test.deleteClientByCPF();
				break;
			case EDIT_CLIENT:
				test.editClientByCPF();
			break;
			case CAD_BANK_DATA:
				test.registerBankData();
				break;
			case LIST_BANK_DATA:
				test.selectAllBankData();
				break;
			case SEARCH_BANK_DATA:
				test.searchBankDataByCPF();
				break;
				
			case DELETE_BANK_DATA:
				test.deleteBankDataByCPF();
				break;
			case EDIT_BANK_DATA:
				test.editBankDataByCPF();
				break;
			case DEPOSIT:
				test.deposit();
				break;
			case SAKE:
					test.sake();
				break;
			default:
				test.exit();
				break;
			}
		}
	}
	
	private void sake() {
		// TODO Auto-generated method stub
		try {
			Client client = new Client(); 
			System.out.print("Digite o cpf: ");
			client.setCpf(readString());
			BankData bankDataSearch =  controller.searchBankDataByCPF(client);
			if(bankDataSearch != null) {
			System.out.print("Digite o valor a ser sacado ");
			bankDataSearch.cash(new BigDecimal(readString()));
			System.out.println("Valor sacado com sucesso");
			System.out.println(bankDataSearch);
			}else {
				System.out.println("Conta nao encontrada");
			}	
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void mock() {
		// TODO Auto-generated method stub
		Client client = new Client("1", "Fulano do Testes");
		BankData bankData = new BankData(client, "1234", "55555", "6", BigDecimal.ZERO);
		controller.addNewClient(client);
		controller.addNewBankData(bankData);
	}

	private void deposit() {
		// TODO Auto-generated method stub
		Client client = new Client(); 
				
		System.out.print("Digite o cpf: ");
		client.setCpf(readString());
		BankData bankDataSearch =  controller.searchBankDataByCPF(client);
		if(bankDataSearch != null) {
		System.out.print("Digite o valor a ser depositado: ");
		bankDataSearch.deposit(new BigDecimal(readString()));
		System.out.println("Valor depositado com sucesso");
		System.out.println(bankDataSearch);
		}else {
			System.out.println("Conta nao encontrada");
		}
	
	}

	private void searchBankDataByCPF() {
		// TODO Auto-generated method stub
		System.out.println("Pesquisar dados bancario por CPF");
		System.out.print("Digite o CPF do Cliente: ");
		String cpfClient = readString();
		Client client = controller.searchClientByCPF(cpfClient);
		if(client != null){
			BankData bankData = controller.searchBankDataByCPF(client);
			System.out.println(bankData);
		}else{
			System.out.println("Cliente n�o encontrado");
		}
	}

	private int readInt() {
		// TODO Auto-generated method stub
		return Integer.parseInt(sc.nextLine());
	}

	private void exit() {
		// TODO Auto-generated method stub
		System.out.println("Sair da Aplica��o");
		if(sc != null){
			sc.close();
		}
		System.exit(0);		
	}

	private void editBankDataByCPF() {
		// TODO Auto-generated method stub
		System.out.println("Editar os dados bancarios pelo CPF do Cliente");
		System.out.print("Digite o CPF do Cliente: ");
		String cpfClient = readString();
		Client client = controller.searchClientByCPF(cpfClient);
		if(client != null){
			System.out.println(client);
			BankData bankData = getInputBankData();
			bankData.setClient(client);
			if(controller.editBankData(bankData)){
				System.out.println("Cliente editado com sucesso");
			}else{
				System.out.println("Erro cliente editar");
			}	
		}else{
			System.out.println("Cliente n�o encontrado");
		}
	}

	private void deleteBankDataByCPF() {
		// TODO Auto-generated method stub
		System.out.println("Apagar os dados bancarios pelo CPF do Cliente");
		String cpfClient = readString();
		Client client = controller.searchClientByCPF(cpfClient);
		if(client != null){
			System.out.println(client);
			if(controller.deleteBankDataByCpf(client) != null){
				System.out.println("Dados Bancario apagado com sucesso");
			}else{
				System.out.println("Conta n�o encontrada");
			}
		}else{
			System.out.println("Cliente n�o encontrado");
		}
		
	}

	private void selectAllBankData() {
		// TODO Auto-generated method stub
		System.out.println("Listar todos os dados banc�rio");
		controller.printBankDatas();
		
	}

	private void registerBankData() {
		// TODO Auto-generated method stub
		System.out.println("Registar Dados Banc�rios");
		System.out.print("Digite o CPF do Cliente: ");
		String cpfClient = readString();
		Client client = controller.searchClientByCPF(cpfClient);
		if(client != null){
			System.out.println(client);
			BankData bankData = getInputBankData();
			bankData.setClient(client);
			controller.addNewBankData(bankData);	
		}else{
			System.out.println("Cliente n�o encontrado");
		}
	}

	private BankData getInputBankData() {
		BankData bankData = new BankData();
		System.out.print("Digite a Agencia: ");
		bankData.setAgency(readString());
		System.out.print("Digite o numero da Conta: ");
		bankData.setAccount(readString());
		System.out.print("Digite o d�gito da Conta: ");
		bankData.setDigit(readString());
		return bankData;
	}

	private void editClientByCPF() {
		// TODO Auto-generated method stub
		System.out.println("Editar Cliente por CPF");
		Client client = getInputClient();
	
		if(controller.editClient(client)){
			System.out.println("Cliente editado com sucesso");
		}else{
			System.out.println("Cliente n�o encontrado");
		}	
	}

	private void deleteClientByCPF() {
		// TODO Auto-generated method stub
		System.out.println("Deletar Cliente por CPF");
		System.out.print("Digite o CPF para ser apagado: ");
		String searchCPF = readString();
		Client client = controller.deleteClientByCPF(searchCPF);
		if(client != null){
			System.out.println(client);
		}else{
			System.out.println("Cliente n�o encontrado");
		}	
		
	}

	private void searchClientByCPF() {
		// TODO Auto-generated method stub
		System.out.println("Pesquisar Cliente por CPF");
		System.out.print("Digite o CPF para a pesquisa: ");
		String searchCPF = readString();
		Client client = controller.searchClientByCPF(searchCPF);
		if(client != null){
			System.out.println(client);
		}else{
			System.out.println("Cliente n�o encontrado");
		}	
	}

	private void selectAllClient() {
		System.out.println("Listar Todos os clientes");
		controller.printClients();
	}

	private void registerClient(){
		System.out.println("Cadastrar Cliente");
		Client clientRegister = getInputClient();
		controller.addNewClient(clientRegister);
	}
	
	private Client getInputClient() {
		// TODO Auto-generated method stub
		Client client = new Client();
		System.out.print("Digite o CPF do Cliente: ");
		client.setCpf(readString());
		System.out.print("Digite o Nome do Cliente: ");
		client.setName(readString());
		return client;
	}

	private String readString(){
		return sc.nextLine();
	}
	
	private void menu(){
		
		Menu[] menus = Menu.values();
    	for(int i=0; i<menus.length; i++){
    		Menu itemMenu = menus[i];
    		System.out.println(String.format("%d - %s", (i+1), itemMenu.getDescription()));
    	}
	}
	
	
	@SuppressWarnings("unused")
	public enum Menu{
		
		CAD_CLIENT("Cadastrar Clientes"),
		LIST_CLIENT("Listar Clientes"),
		SEARCH_CLIENT_CPF("Buscar Clientes por CPF"),
		DELETE_CLIENT("Apagar Clientes por CPF"),
		EDIT_CLIENT("Editar Clientes"),
		CAD_BANK_DATA("Cadastrar dados banc�rio"),
		LIST_BANK_DATA("Listar todos os Dados Banc�rio"),
		SEARCH_BANK_DATA("Buscar dados ban�rio por CPF"),
		DELETE_BANK_DATA("Apagar dados bancario por CPF"),
		EDIT_BANK_DATA("Editar Dados Banc�rios"),
		DEPOSIT("Depositar por CPF do Cliente"),
		 SAKE("Sacar por CPF do Cliente"),
		 EXIT("Sair");
	
		private String description;

		Menu(String desc){
			this.description = desc;
		}
		
		public String getDescription() {
			return description;
		}
		
		public static Menu getOptionToMenu(int option){
			option-=1;
			if(option > Menu.values().length || option < 0){
				return Menu.EXIT;
			}
			return Menu.values()[option];
		}

	}
	
}
