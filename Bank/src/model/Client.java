package model;

public class Client {
	
	private String cpf;
	private String name;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return String.format("CPF: %s, Nome: %s" , cpf, name);
	}
	public Client() {
		super();
	}
	public Client(String cpf, String name) {
		super();
		this.cpf = cpf;
		this.name = name;
	}
	
	
	
}
