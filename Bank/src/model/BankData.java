package model;

import java.math.BigDecimal;

public class BankData {

	private Client client;
	private String agency;
	private String account;
	private String digit;
	private BigDecimal value = BigDecimal.ZERO;

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDigit() {
		return digit;
	}

	public void setDigit(String digit) {
		this.digit = digit;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public void deposit(BigDecimal addValue) {
		if (addValue.doubleValue() > 0) {
			value = value.add(addValue);
		}
	}

	public BankData() {
	}

	public BankData(Client client, String agency, String account, String digit, BigDecimal value) {
		this.client = client;
		this.agency = agency;
		this.account = account;
		this.digit = digit;
		this.value = value;
	}

	public void cash(BigDecimal valueCash) throws RuntimeException {
		
		if (valueCash.compareTo(BigDecimal.ZERO) <= 0 ) {
			throw new RuntimeException("Erro: Valor Zerado");
		}
		
		System.out.println("Number: " + valueCash.compareTo(this.value));	

		if (valueCash.compareTo(this.value) == 1) {
			throw new RuntimeException("Erro: Valor Acima do que foi permitido");
		}
		
		
		
		
	
		value = value.subtract(valueCash);
	
		
	}

	@Override
	public String toString() {
		return "BankData [client=" + client + ", agency=" + agency + ", account=" + account + ", digit=" + digit
				+ ", value=" + value + "]";
	}

}
