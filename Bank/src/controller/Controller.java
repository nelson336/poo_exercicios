package controller;

import java.util.ArrayList;
import java.util.List;

import model.BankData;
import model.Client;

public class Controller {
	
	private List<Client> clients = new ArrayList<>();
	private List<BankData> bankDatas = new ArrayList<>();
	
	public void addNewClient(Client client) {
		// TODO Auto-generated method stub
			clients.add(client);
	}

	public void printClients() {
		// TODO Auto-generated method stub
		for(Client client : clients){
			System.out.println(client);
		}
		
	}

	public Client searchClientByCPF(String searchCPF) {
		
		for(Client client : clients){
			if(client.getCpf().equalsIgnoreCase(searchCPF)){
				return client;
			}
		}
		
		return null;
	}

	public Client deleteClientByCPF(String searchCPF) {
		// TODO Auto-generated method stub
		for(Client client : clients){
			if(client.getCpf().equalsIgnoreCase(searchCPF)){
			  clients.remove(client);
			  return client;
			}
		}
		return null;
	}

	public boolean editClient(Client clientNew) {
		// TODO Auto-generated method stub
		
		for(int i = 0; i < clients.size();	i++){
			Client client = clients.get(i);
			if(client.getCpf().equalsIgnoreCase(clientNew.getCpf())){
			  clients.set(i, clientNew);
			  return true;
			}
		}
		
		
		return false;
	}

	public void addNewBankData(BankData bankData) {
		// TODO Auto-generated method stub
		if(bankData != null){
			bankDatas.add(bankData);
		}
		
	}
	
	
	public void printBankDatas() {
		// TODO Auto-generated method stub
		for(BankData bankData : bankDatas){
			System.out.println(bankData);
		}
		
	}

	public boolean editBankData(BankData bankData) {
		// TODO Auto-generated method stub
		for(int i=0; i< bankDatas.size(); i++){
			BankData bankDataItem = bankDatas.get(i);
			if(bankDataItem.getClient() != null && bankDataItem.getClient().getCpf().equalsIgnoreCase(bankData.getClient().getCpf())){
				bankDatas.set(i, bankData);
				return true;
			}
		}
		
		return false;
	}

	public BankData searchBankDataByCPF(Client client) {
		// TODO Auto-generated method stub
		for(BankData bankDataItem : bankDatas){
			if(bankDataItem.getClient() != null && bankDataItem.getClient().getCpf().equalsIgnoreCase(bankDataItem.getClient().getCpf())){
				return bankDataItem;
			}
			
		}
		
		return null;
	}

	public BankData deleteBankDataByCpf(Client client) {
		// TODO Auto-generated method stub
		
		for(BankData bankData : bankDatas){
			if(bankData.getClient() != null && bankData.getClient().getCpf().equalsIgnoreCase(client.getCpf())){
				bankDatas.remove(bankData);
				return bankData;
			}		
		}
		
		return null;
	}

}
