package test;

import java.util.Scanner;

import data.BaseDados;
import model.Cd;
import model.Video;

public class Test {
	
	private Scanner sc = new Scanner(System.in);
	private BaseDados data = new BaseDados();
	
	public static void main(String[] args) {
		new Test().test();
	}
	
	private void mock() {
		data.inserir(new Cd(data.generateId(), "Esse Cd n�o � bom", "Cd01", 100, true, "Fulano de Tal", 5));
		data.inserir(new Cd(data.generateId(), "Esse cd tbm n e bom", "Cd02", 500, false, "Beltrano", 20));
		data.inserir(new Video(data.generateId(), "Esse Dvd � mais ou menos", "Filme 01", 500, true, "Beltrano"));
		data.inserir(new Video(data.generateId(), "Esse dvd � pirata", "Filme02", 500, false, "Ciclano"));
	}

	private void test() {
		mock();
		while(true) {
			menu();
			System.out.print("Digite uma opcao: ");
			int op = readInt();
			switch (op) {
			case 1:
				inserirCd();
				break;
			case 2:
				listarCds();
				break;
			case 3:
				editarCds();
			break;
			case 4:
				inserirVideo();
			break;
			case 5:
				listarVideos();
			break;
			case 6:
				editarVideos();
			break;
			case 7:
				apagar();
			break;
			default:
				sair();
				break;
			}
		}
	}
	
	private void editarVideos() {
		// TODO Auto-generated method stub
		Video video = new Video();
		video.scanner(sc);
		System.out.println("Digite o id que voce deseja editar: ");
		int id = readInt();
		video.setId(id);
		data.atualizar(id, video);	
	}

	private void listarVideos() {
		// TODO Auto-generated method stub
		data.listarVideos();
	}

	private void inserirVideo() {
		// TODO Auto-generated method stub
		Video video = new Video();
		video.setId(data.generateId());
		video.scanner(sc);
		data.inserir(video);
	}

	private void apagar() {
		// TODO Auto-generated method stub
		System.out.println("Digite o id que voce deseja apagar: ");
		int id = readInt();
		data.apagar(id);
	}

	private void editarCds() {
		// TODO Auto-generated method stub
		Cd newCd = new Cd();
		newCd.scanner(sc);
		System.out.println("Digite o id que voce deseja editar: ");
		int id = readInt();
		newCd.setId(id);
		data.atualizar(id, newCd);	
	}

	private void sair() {
		// TODO Auto-generated method stub
		sc.close();
		System.exit(0);
	}

	private void listarCds() {
		// TODO Auto-generated method stub
		data.listarCds();
	}

	private void inserirCd() {
		// TODO Auto-generated method stub
		Cd cd = new Cd();
		cd.setId(data.generateId());
		cd.scanner(sc);
		data.inserir(cd);
	}

	private int readInt() {
		return Integer.parseInt(sc.nextLine());
	}

	private void menu() {
		// TODO Auto-generated method stub
		System.out.println("1 - Inserir Cds");
		System.out.println("2 - Listar Cds");
		System.out.println("3 - Editar Cds");
		System.out.println("4 - Inserir Video");
		System.out.println("5 - Listar Videos");
		System.out.println("6 - Editar Video");
		System.out.println("7 - Apagar por Id");
		
	}

}
