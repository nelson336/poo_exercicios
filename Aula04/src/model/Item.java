package model;

import java.util.Scanner;

public abstract class Item {
	
	protected int id = -1;
	protected String comentario;
	protected String titulo;
	protected int tempoDuracao;
	protected boolean emprestado;
	
	public boolean isEmprestado() {
		return emprestado;
	}
	public void setEmprestado(boolean emprestado) {
		this.emprestado = emprestado;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getTempoDuracao() {
		return tempoDuracao;
	}
	public void setTempoDuracao(int tempoDuracao) {
		this.tempoDuracao = tempoDuracao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public void scanner(Scanner sc) {
		System.out.print("Digite o titulo: ");
		titulo = sc.nextLine();
		System.out.print("Digite o comentario: ");
		comentario = sc.nextLine();
		System.out.print("E emprestado? (s/n): ");
		emprestado = sc.nextLine().trim().equalsIgnoreCase("s") ? true : false;
		System.out.print("Digite o tempo de dura��o: ");
		tempoDuracao = Integer.parseInt(sc.nextLine());
	}
	
	
	
	

}
