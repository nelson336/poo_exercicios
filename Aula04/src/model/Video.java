package model;

import java.util.Scanner;

public class Video extends Item {

	private String diretor;

	@Override
	public void scanner(Scanner sc) {
		// TODO Auto-generated method stub
		super.scanner(sc);
		System.out.print("Digite o diretor: ");
		diretor = sc.nextLine();
	}

	public Video() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Video(int id, String comentario, String titulo, int tempoDuracao, boolean emprestado, String diretor) {
		this.id = id;
		this.comentario = comentario;
		this.titulo = titulo;
		this.tempoDuracao = tempoDuracao;
		this.emprestado = emprestado;
		this.diretor = diretor;
	}

	@Override
	public String toString() {
		return "Video [diretor=" + diretor + ", id=" + id + ", comentario=" + comentario + ", titulo=" + titulo
				+ ", tempoDuracao=" + tempoDuracao + ", emprestado=" + emprestado + "]";
	} 
	
}
