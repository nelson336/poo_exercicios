package model;

import java.util.Scanner;

public class Cd extends Item {

	private String artista;
	private int nroMusicas;
	
	public String getArtista() {
		return artista;
	}
	public void setArtista(String artista) {
		this.artista = artista;
	}
	public int getNroMusicas() {
		return nroMusicas;
	}
	public void setNroMusicas(int nroMusicas) {
		this.nroMusicas = nroMusicas;
	}
	
	@Override
	public void scanner(Scanner sc) {
		// TODO Auto-generated method stub
		super.scanner(sc);
		System.out.print("Digite o artista: ");
		artista = sc.nextLine();
		System.out.print("Digite a quantidade de musicas: ");
		nroMusicas = Integer.parseInt(sc.nextLine());
		
	}
	
	
	public Cd(int id, String comentario, String titulo, int tempoDuracao, boolean emprestado, String artista, int nroMusica) {
		this.id = id;
		this.comentario = comentario;
		this.titulo = titulo;
		this.tempoDuracao = tempoDuracao;
		this.emprestado = emprestado;
		this.artista = artista;
		this.nroMusicas = nroMusica;
	} 
	
	public Cd() {
		//
	}
	
	@Override
	public String toString() {
		return "Cd [artista=" + artista + ", nroMusicas=" + nroMusicas + ", id=" + id + ", comentario=" + comentario
				+ ", titulo=" + titulo + ", tempoDuracao=" + tempoDuracao + ", emprestado=" + emprestado + "]";
	}
	
}
