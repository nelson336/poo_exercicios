package data;

import java.util.ArrayList;
import java.util.List;

import model.Cd;
import model.Item;
import model.Video;

public class BaseDados {

	private List<Item> items = new ArrayList<>();

	public void inserir(Item item) {
		// TODO Auto-generated method stub
		if (item != null) {
			items.add(item);
		}
	}

	public void apagar(int id) {
		if (id > -1) {
			for (Item search : items) {
				if (search.getId() == id) {
					items.remove(search);
					return;
				}
			}

			System.out.println("Item n�o encontrado");
		}
	}

	public void atualizar(int id, Item item) {
		if (id > -1) {
			for (int i=0; i<items.size(); i++) {
				Item search = items.get(i);
				if (search.getId() == id) {
					items.set(i, item);
					return;
				}
			}

			System.out.println("Item n�o encontrado");
		}
	}

	public void listarCds() {
		for (Item search : items) {
			 if(search instanceof Cd) {
				System.out.println(((Cd)search));
			}
		}
	}
	
	public void listarVideos() {
		for (Item search : items) {
			if(search instanceof Video) {
				System.out.println(((Video)search));
			}
		}
	}
	
	public int generateId() {
		return items.size();
	}

}
